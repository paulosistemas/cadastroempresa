package br.com.controllers;

import br.com.models.Empresa;
import br.com.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    EmpresaService empresaService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void postarEmpresa(@RequestBody Empresa empresa){
        empresaService.postarEmpresa(empresa);
    }
}
