package br.com.services;

import br.com.models.Empresa;
import br.com.producers.EmpresaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    EmpresaProducer empresaProducer;

    public void postarEmpresa(Empresa empresa){

        empresaProducer.enviarAoKafka(empresa);


    }


}
